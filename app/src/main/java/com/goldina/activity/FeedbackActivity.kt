package com.goldina.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class FeedbackActivity : AppCompatActivity() {
    private lateinit var viewModel: FeedbackViewModel
    private lateinit var buttonSave: Button
    private lateinit var editText: EditText
    private lateinit var textView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feedback)

        buttonSave = findViewById(R.id.button)
        editText = findViewById(R.id.editTextTextMultiLine)
        textView = findViewById(R.id.textViewFeedback)
        viewModel = ViewModelProvider(this).get(FeedbackViewModel::class.java)

        viewModel.currentText.observe(this, Observer {
            textView.text=it.toString()
        })

        saveFeedback()
    }
    private fun saveFeedback (){
        buttonSave.setOnClickListener {
            textView.text = editText.text.toString()
            viewModel.currentText.value= textView.text.toString()

        }

    }
}