package com.goldina.activity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class FeedbackViewModel: ViewModel() {
    val currentText:MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
}