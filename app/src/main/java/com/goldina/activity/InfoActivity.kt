package com.goldina.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import java.util.*

fun String.capitalizeWords(): String = split(" ").joinToString(" ") { it.replaceFirstChar {
    if (it.isLowerCase()) it.titlecase(
        Locale.getDefault()
    ) else it.toString()
} }

class InfoActivity : AppCompatActivity() {
    private lateinit var textViewInfo: TextView
    private lateinit var imageView: ImageView

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        textViewInfo = findViewById(R.id.textViewInfo)
        imageView = findViewById(R.id.imageView2)
        val lastName = intent.getStringExtra(PersonInfo.lastName)
        val name = intent.getStringExtra(PersonInfo.name)
        val middleName:String? = intent.getStringExtra(PersonInfo.middleName)
        val age = intent.getStringExtra(PersonInfo.age)

        val infoString = "$lastName $name $middleName \nВаш возраст: $age\n".capitalizeWords()
        when(age?.toInt()){
            in 0..25 ->{
                imageView.setImageResource(R.drawable.genz)
                textViewInfo.text = infoString+getString(R.string.genZ)
            }
            in 26..41 ->{
                imageView.setImageResource(R.drawable.geny)
                textViewInfo.text = infoString+getString(R.string.genY)

            }
            in 42..100->{
                imageView.setImageResource(R.drawable.genx)
                textViewInfo.text = infoString+getString(R.string.genX)
            }
        }
    }
}