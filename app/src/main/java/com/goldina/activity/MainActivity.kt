package com.goldina.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    private lateinit var editTextLastName: EditText
    private lateinit var editTextName: EditText
    private lateinit var editTextMiddleName: EditText
    private lateinit var editTextAge: EditText
    private lateinit var editTextHobby: EditText
    private lateinit var buttonNext: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        buttonNext=findViewById(R.id.buttonNext)
        editTextLastName=findViewById(R.id.editTextLastName)
        editTextName = findViewById(R.id.editTextName)
        editTextMiddleName = findViewById(R.id.editTextMiddleName)
        editTextAge = findViewById(R.id.editTextAge)
        editTextHobby = findViewById(R.id.editTextHobby)

        loadData()

        buttonNext.setOnClickListener {
            Intent(this@MainActivity, InfoActivity::class.java).also {
                it.putExtra(PersonInfo.lastName, editTextLastName.text.toString())
                it.putExtra(PersonInfo.name, editTextName.text.toString())
                it.putExtra(PersonInfo.middleName, editTextMiddleName.text.toString())
                it.putExtra(PersonInfo.age, editTextAge.text.toString())
                it.putExtra(PersonInfo.descrpHobby, editTextHobby.text.toString())
                startActivity(it)
            }
        }


    }
    override fun onCreateOptionsMenu(menu:Menu?):Boolean{
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

     override fun onOptionsItemSelected(item:MenuItem):Boolean{
        when(item.itemId){
            android.R.id.home -> finish()
            R.id.save -> saveData()
            R.id.change ->{
                changeBackground()
                Toast.makeText(this,"Фон изменен",Toast.LENGTH_SHORT).show()
            }
            R.id.open ->
                Intent(this@MainActivity, FeedbackActivity::class.java).also {
                    startActivity(it)
                }
            R.id.clear ->{
                val sharedPreferences = getSharedPreferences("sharedPref",Context.MODE_PRIVATE)
                sharedPreferences.edit().clear().apply()
                Toast.makeText(this,"Кэш удален",Toast.LENGTH_SHORT).show()
            }
        }
        return true
    }
    private fun saveData(){
        val sharedPreferences = getSharedPreferences("sharedPref",Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putString("lastName",editTextLastName.text.toString())
            putString("Name",editTextName.text.toString())
            putString("middleName",editTextMiddleName.text.toString())
            putString("Age",editTextAge.text.toString())
            putString("Hobby",editTextHobby.text.toString())
        }.apply()
        Toast.makeText(this,"Сохранено",Toast.LENGTH_SHORT).show()
    }
    private fun loadData(){
        val sharedPreferences = getSharedPreferences("sharedPref",Context.MODE_PRIVATE)
        editTextLastName.setText(sharedPreferences.getString("lastName",null))
        editTextName.setText(sharedPreferences.getString("Name",null))
        editTextMiddleName.setText(sharedPreferences.getString("middleName",null))
        editTextAge.setText(sharedPreferences.getString("Age",null))
        editTextHobby.setText(sharedPreferences.getString("Hobby",null))
    }

    private fun changeBackground() {
        val view:ConstraintLayout = findViewById(R.id.activity_main)
        Random.nextInt(4).let {
            when (it) {
                0 -> view.setBackgroundResource(R.drawable.bckgrd_first)
                1 -> view.setBackgroundResource(R.drawable.bckgrd_second);
                2 -> view.setBackgroundResource(R.drawable.bckgrd_third);
                3 -> view.setBackgroundResource(R.drawable.bckgrd_fourth);
                }
            }
        }
    }